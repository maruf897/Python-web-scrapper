import scrapy
import time
import ebay_extractor

# this is the pagination bot
class EbaybotSpider(scrapy.Spider):
    name = "ebaybot"

    start_urls = [
        "https://www.ebay.com/b/Laptops-Netbooks/175672/bn_1648276?rt=nc&_pgn=1"
    ]
    page = 1

    def parse(self, response):
        # selector format "element.classname"
        item_link_selector = "a.s-item__link"
        next_page_link_selector = "a.pagination__next"
        for i, a in enumerate(response.css(item_link_selector)):
            print(a.attrib["href"])
            # custom extractor for webpage goes here
            ebay_extractor.Extractor(a.attrib["href"], i, self.page)
            time.sleep(1)

        next_page = response.css(next_page_link_selector).attrib["href"]
        if next_page is not None:
            self.page = self.page + 1

            yield response.follow(next_page, callback=self.parse)
