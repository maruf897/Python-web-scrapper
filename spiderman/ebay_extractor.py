from bs4 import BeautifulSoup
import requests
from datetime import datetime

# this extractor function will be different for different page
def Extractor(url, index, page):
    # ebaay has different slector in different page
    title_selector = {"element": "h1", "class": "product-title"}
    alt_title_selector = {"element": "h1", "class": "it-ttl"}
    price_selector = {"element": "span", "class": "item-price"}
    alt_price_selector = {"element": "span", "class": "notranslate"}
    specs_selector = {"element": "div", "class": "ux-labels-values__labels-content"}
    specs_content_selector = {
        "element": "div",
        "class": "ux-labels-values__values-content",
    }

    req = requests.get(url)
    print(type(title_selector["class"]))
    soup = BeautifulSoup(req.text, "lxml")
    body = soup.body
    try:
        title = body.find(
            alt_title_selector["element"], {"class": alt_title_selector["class"]}
        ).text
    except:
        try:
            title = body.find(
                title_selector["element"], {"class": title_selector["class"]}
            ).text
        except:
            title = "not found"

    try:
        price = body.find(
            price_selector["element"], {"class": price_selector["class"]}
        ).text
    except:
        try:
            price = body.find(
                alt_price_selector["element"], {"class": alt_price_selector["class"]}
            ).text
        except:
            price = "not found"
    with open(
        "product" + str(index) + str(page) + ".txt", "a", encoding="utf-8"
    ) as file1:
        file1.write("Title: " + title + "\n")
        file1.write("Price: " + price + "\n")
        file1.write("Specs Data: " + "\n")
    # this is for the specs part in the details page
    specs_labels = body.find_all(
        specs_selector["element"], {"class": specs_selector["class"]}
    )
    specs_content_labels = body.find_all(
        specs_content_selector["element"], {"class": specs_content_selector["class"]}
    )
    for i, labels in enumerate(specs_labels):
        print(labels.text)
        with open(
            "product" + str(index) + str(page) + ".txt", "a", encoding="utf-8"
        ) as file1:
            file1.write(labels.text + " " + specs_content_labels[i].text + "\n")

    # for any table
    tables = body.find_all("table")
    rows = []
    if len(tables) > 0:
        for table in tables:
            rows = table.find_all("tr")
            table_head = table.find_all("th")

        with open(
            "product" + str(index) + str(page) + ".txt", "a", encoding="utf-8"
        ) as file1:

            file1.write("Table Data \n")
            if len(table_head) > 0:
                for th in table_head:
                    file1.write(th.text + ",")
                file1.write("\n")
            if len(rows) > 0:
                for row in rows:
                    cols = row.find_all("td")
                    for td in cols:
                        file1.write(td.text + ",")
                    file1.write("\n")
    # for any image in the details page
    links = body.find_all("img", {"src": True})

    accepted_format = (".pdf", ".docx", ".mp3", ".jpg", ".png", ".jpeg")
    for link in links:
        if link["src"] is not None:
            doc_link = link["src"]

            if doc_link.endswith((accepted_format)):
                r = requests.get(doc_link, allow_redirects=True)
                print(doc_link)
                doc_name = doc_link.rsplit("/", 1)[1]
                with open(
                    "product" + str(index) + str(page) + ".txt", "a", encoding="utf-8"
                ) as file1:
                    file1.write("\nFiles Paths: ")
                    file1.write(str(page) + doc_name)
                with open(str(page) + doc_name, "wb") as file2:
                    file2.write(r.content)
